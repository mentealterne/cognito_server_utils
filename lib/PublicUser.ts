import * as AmazonCognitoIdentity from "amazon-cognito-identity-js";
import * as interfaces from "../lib/interfaces";
import * as AWS from "aws-sdk";
import { CognitoOnServer } from "./CognitoOnServer";
export class PublicCognitoUser extends CognitoOnServer {
  public userSignup(
    username: string,
    password: string,
    userAttributes: AmazonCognitoIdentity.ICognitoUserAttributeData[]
  ): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this.poolAuth().signUp(
        username,
        password,
        this.makeCognitoObjects(userAttributes),
        this.makeCognitoObjects(userAttributes),
        (err: any, results: any) => {
          if (err) reject(err);
          reject(err);
          resolve(results);
        }
      );
    });
  }
  public confirmUserInPool(
    username: string,
    confirmationCode: string
  ): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      AWS.config.region = this.region;

      var cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider();
      var params = {
        ClientId: this.clientId,
        ConfirmationCode: confirmationCode,
        Username: username,
        ForceAliasCreation: false
      };

      cognitoIdentityServiceProvider.confirmSignUp(params, function(
        err,
        results
      ) {
        if (err) reject(err);
        resolve(results);
      });
    });
  }

  public userPasswordResetRequest(username: string): Promise<any> {
    return new Promise((resolve: any, reject: any) => {
      this.cognitoUserInstance(username).forgotPassword({
        onSuccess: data => {
          resolve(data);
        },
        onFailure: error => {
          reject(error);
        }
      });
    });
  }

  public userNewPassword(
    username: string,
    verificationCode: string,
    newPassword: string
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      this.cognitoUserInstance(username).confirmPassword(
        verificationCode,
        newPassword,
        {
          onSuccess: () => {
            resolve(true);
          },
          onFailure: error => {
            resolve(error);
          }
        }
      );
    });
  }

  public userEditAttributes(
    username: string,
    attributes: AmazonCognitoIdentity.ICognitoUserAttributeData[]
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      this.cognitoUserInstance(username).updateAttributes(
        attributes,
        (err, data) => {
          if (err) reject(err);
          resolve(data);
        }
      );
    });
  }
}
