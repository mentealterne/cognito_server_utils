import * as AmazonCognitoIdentity from "amazon-cognito-identity-js";
import * as interfaces from "../lib/interfaces";
import * as AWS from "aws-sdk";
export class CognitoOnServer {
  userPoolId: string;
  clientId: string;
  identityPoolId: string;
  loginId: string;
  region: string;
  constructor(configObject: interfaces.configObject) {
    this.userPoolId = configObject.userPoolId;
    this.clientId = configObject.clientId;
    this.identityPoolId = configObject.identityPoolId;
    this.loginId = configObject.loginId;
    this.region = configObject.region;
  }

  public cognitoUserInstance(
    username: string
  ): AmazonCognitoIdentity.CognitoUser {
    var userData = {
      Username: username,
      Pool: this.poolAuth()
    };
    return new AmazonCognitoIdentity.CognitoUser(userData);
  }

  public poolAuth(): AmazonCognitoIdentity.CognitoUserPool {
    return new AmazonCognitoIdentity.CognitoUserPool({
      UserPoolId: this.userPoolId,
      ClientId: this.clientId
    });
  }
  public makeCognitoObjects(
    attributesArray: AmazonCognitoIdentity.ICognitoUserAttributeData[]
  ): AmazonCognitoIdentity.CognitoUserAttribute[] {
    return attributesArray.map(
      e => new AmazonCognitoIdentity.CognitoUserAttribute(e)
    );
  }

  public login(Username: string, Password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.cognitoUserInstance(Username).authenticateUser(
        new AmazonCognitoIdentity.AuthenticationDetails({ Username, Password }),
        {
          onSuccess: (result: AmazonCognitoIdentity.CognitoUserSession) => {
            const accessToken = result.getAccessToken().getJwtToken();
            const idToken = result.getIdToken().getJwtToken();

            resolve({ type: "accessAuthInfo", result });
          },
          onFailure: error => {
            reject(error);
          },
          newPasswordRequired: (userAttributes, requiredAttributes) => {
            resolve({
              type: "newPasswordChallenge",
              userAttributes,
              requiredAttributes
            });
          }
        } as AmazonCognitoIdentity.IAuthenticationCallback
      );
    });
  }
}
