export interface userData {
  name: string;
  value: string;
}
export interface configObject {
  userPoolId: string;
  clientId: string;
  identityPoolId: string;
  loginId: string;
  region: string;
}
