"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var AWS = __importStar(require("aws-sdk"));
var CognitoOnServer_1 = require("./CognitoOnServer");
var PublicCognitoUser = /** @class */ (function (_super) {
    __extends(PublicCognitoUser, _super);
    function PublicCognitoUser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PublicCognitoUser.prototype.userSignup = function (username, password, userAttributes) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.poolAuth().signUp(username, password, _this.makeCognitoObjects(userAttributes), _this.makeCognitoObjects(userAttributes), function (err, results) {
                if (err)
                    reject(err);
                reject(err);
                resolve(results);
            });
        });
    };
    PublicCognitoUser.prototype.confirmUserInPool = function (username, confirmationCode) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            AWS.config.region = _this.region;
            var cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider();
            var params = {
                ClientId: _this.clientId,
                ConfirmationCode: confirmationCode,
                Username: username,
                ForceAliasCreation: false
            };
            cognitoIdentityServiceProvider.confirmSignUp(params, function (err, results) {
                if (err)
                    reject(err);
                resolve(results);
            });
        });
    };
    // TODO USER RESET PASSWORD
    PublicCognitoUser.prototype.userPasswordResetRequest = function (username) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.cognitoUserInstance(username).forgotPassword({
                onSuccess: function (data) {
                    resolve(data);
                },
                onFailure: function (error) {
                    reject(error);
                }
            });
        });
    };
    PublicCognitoUser.prototype.userNewPassword = function (username, verificationCode, newPassword) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.cognitoUserInstance(username).confirmPassword(verificationCode, newPassword, {
                onSuccess: function () {
                    resolve(true);
                },
                onFailure: function (error) {
                    resolve(error);
                }
            });
        });
    };
    PublicCognitoUser.prototype.userEditAttributes = function (username, attributes) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.cognitoUserInstance(username).updateAttributes(attributes, function (err, data) {
                if (err)
                    reject(err);
                resolve(data);
            });
        });
    };
    return PublicCognitoUser;
}(CognitoOnServer_1.CognitoOnServer));
exports.PublicCognitoUser = PublicCognitoUser;
