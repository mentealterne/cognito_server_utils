"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var AmazonCognitoIdentity = __importStar(require("amazon-cognito-identity-js"));
var CognitoOnServer = /** @class */ (function () {
    function CognitoOnServer(configObject) {
        this.userPoolId = configObject.userPoolId;
        this.clientId = configObject.clientId;
        this.identityPoolId = configObject.identityPoolId;
        this.loginId = configObject.loginId;
        this.region = configObject.region;
    }
    CognitoOnServer.prototype.cognitoUserInstance = function (username) {
        var userData = {
            Username: username,
            Pool: this.poolAuth()
        };
        return new AmazonCognitoIdentity.CognitoUser(userData);
    };
    CognitoOnServer.prototype.poolAuth = function () {
        return new AmazonCognitoIdentity.CognitoUserPool({
            UserPoolId: this.userPoolId,
            ClientId: this.clientId
        });
    };
    CognitoOnServer.prototype.makeCognitoObjects = function (attributesArray) {
        return attributesArray.map(function (e) { return new AmazonCognitoIdentity.CognitoUserAttribute(e); });
    };
    CognitoOnServer.prototype.login = function (Username, Password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.cognitoUserInstance(Username).authenticateUser(new AmazonCognitoIdentity.AuthenticationDetails({ Username: Username, Password: Password }), {
                onSuccess: function (result) {
                    var accessToken = result.getAccessToken().getJwtToken();
                    var idToken = result.getIdToken().getJwtToken();
                    resolve({ type: "accessAuthInfo", result: result });
                },
                onFailure: function (error) {
                    reject(error);
                },
                newPasswordRequired: function (userAttributes, requiredAttributes) {
                    resolve({
                        type: "newPasswordChallenge",
                        userAttributes: userAttributes,
                        requiredAttributes: requiredAttributes
                    });
                }
            });
        });
    };
    return CognitoOnServer;
}());
exports.CognitoOnServer = CognitoOnServer;
